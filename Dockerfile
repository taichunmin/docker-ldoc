FROM ubuntu:16.04
LABEL maintainer="taichunmin@gmail.com"
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=C.UTF-8 REFRESHED_AT=2017-11-18 DEBIAN_FRONTEND=noninteractive
WORKDIR /root

RUN apt-get update -qqy && \
    apt-get install -y lua5.1 luarocks && \
    luarocks install LuaFileSystem && \
    luarocks install penlight && \
    luarocks install lua-discount && \
    luarocks install ldoc && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["tail", "-f", "/etc/issue"]
